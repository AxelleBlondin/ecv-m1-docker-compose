# Docker Compose
Create the image : 
```
docker-compose build
```
Then, launch the container with this command : 
```
docker-compose up -d
```
We need to add Composer dependency to the image 
```
docker-compose exec app composer install
```
We now have Symfony on this URL : [http://localhost:8080](http://localhost:8080)
## Symfony
### Requirements
- MariaDB 10: an available MariaDB server running on port `3306` and host `db` with the username as `symfony`, the password as `symfony` and a database named `symfony`.

### How to run
Copy the environment file
```sh
cp .env .env.local
```
Create the database if it does not exist
```sh
php bin/console doctrine:database:create --if-not-exists
```

Create the database schema
```sh
php bin/console doctrine:schema:update
```
